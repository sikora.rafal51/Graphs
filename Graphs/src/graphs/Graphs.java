
package graphs;

import java.util.ArrayList;

class Vertex {

    String name;
    int cost = Integer.MAX_VALUE;
    Vertex predecessor = this;
    boolean isDone = false;

    ArrayList<Edge> edges;

    Vertex(String name) {
        this.edges = new ArrayList<>();
        this.name = name;
    }

    @Override
    public String toString() {
        String result = name + " (" + predecessor.name + ", " + cost + ")\n ";
        if (edges.isEmpty()) {
            return result + "No edges";
        }
        for (Edge k : edges) {
            result += k + " ";
        }
        return result;
    }

    void add(Vertex vertex, int weight) {
        edges.add(new Edge(this, vertex, weight));
    }

    void edgeRelaxation() {
        for (Edge ed : edges) {
            if (cost + ed.weight < ed.k.cost) {
                ed.k.cost = cost + ed.weight;
                ed.k.predecessor = this;
            }
        }
        isDone = true;
    }
}

class Edge {

    Vertex p, k;
    int weight;

    Edge(Vertex p, Vertex k, int weight) {

        this.p = p;
        this.k = k;
        this.weight = weight;

    }

    @Override
    public String toString() {
        return "->" + k.name + "[" + weight + "],";
    }

}

class Graph {

    ArrayList<Vertex> vertices = new ArrayList<>();

    @Override
    public String toString() {
        String result = "";
        for (Vertex v : vertices) {
            result += v + "\n";
        }
        return result;
    }

    void add(String name) {
        vertices.add(new Vertex(name));

    }

    void add(Vertex vertex) {
        vertices.add(vertex);

    }

    Vertex min() {
        Vertex result = null;
        int m = Integer.MAX_VALUE;
        for (Vertex v : vertices) {
            if (!v.isDone && v.cost < m) {
                m = v.cost;
                result = v;

            }
        }
        return result;
    }

    void dijkstraAlgorithm() {
        // Starting from the 4th vertex
        vertices.get(0).cost = 0;
        Vertex min = min();
        while (min != null) {
            min.edgeRelaxation();
            min = min();
        }
    }
}

public class Graphs {

    public static void main(String[] args) {

        /*Graph graph = new Graph();
        Vertex v1 = new Vertex("v1");
        Vertex v2 = new Vertex("v2");
        Vertex v3 = new Vertex("v3");
        Vertex v4 = new Vertex("v4");

        graph.add(v1);
        graph.add(v2);
        graph.add(v3);
        graph.add(v4);

        v1.add(v2, 10);
        v1.add(v3, 100);
        v1.add(v4, 50);
        v2.add(v4, 20);
        v4.add(v3, 30);
        
        System.out.println(graph);
        graph.dijkstraAlgorithm();
        System.out.println(graph);
        */
    }

}
